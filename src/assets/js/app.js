import $ from 'jquery';
import 'what-input';

// Foundation JS relies on a global variable. In ES6, all imports are hoisted
// to the top of the file so if we used `import` to import Foundation,
// it would execute earlier than we have assigned the global variable.
// This is why we have to use CommonJS require() here since it doesn't
// have the hoisting behavior.
window.jQuery = $;
require('foundation-sites');

// If you want to pick and choose which modules to include, comment out the above and uncomment
// the line below
//import './lib/foundation-explicit-pieces';


$(document).foundation();








    
// new ScrollMagic.Scene({
//       triggerElement: ("#section-1"),
//       triggerHook: "onLeave",
//       duration: '0',

//     })


//    .add(TweenMax.to(obj, 2,
//        {
//            curImg: grillopen.length - 1,	// animate propery curImg to number of images
//            roundProps: "curImg",				// only integers so it can be used as an array index
//            repeat: 0,									// repeat 0 times
//            immediateRender: true,			// load first image automatically
//            ease: Linear.easeNone,			// show every image the same ammount of time
//            onUpdate: function () {
//                $("#grill-img").attr("src", grillopen[obj.curImg]); // set the image source
//            }, delay: -3
//        }
//    ))

      // .add(TweenMax.to(obj, 1,
      //   {
      //     curImg: fire_images.length - 1,	// animate propery curImg to number of images
      //     roundProps: "curImg",				// only integers so it can be used as an array index
      //     repeat: 10,									// repeat 3 times
      //     immediateRender: true,			// load first image automatically
      //     ease: Linear.easeNone,			// show every image the same ammount of time
      //     onUpdate: function () {
      //       $("#grill-img").attr("src", fire_images[obj.curImg]); // set the image source
      //     }
      //   }
      // ))


   // var fireanimation = new TimelineMax()
    //   .add(TweenMax.to(obj, 2,
    //     {
    //       curImg: fire_images.length - 1,	// animate propery curImg to number of images
    //       roundProps: "curImg",				// only integers so it can be used as an array index
    //       repeat: 10,									// repeat 3 times
    //       immediateRender: true,			// load first image automatically
    //       ease: Linear.easeNone,			// show every image the same ammount of time
    //       onUpdate: function () {
    //         $("#grill-img").attr("src", fire_images[obj.curImg]); // set the image source
    //       }
    //     }
    //   ))








// import LocomotiveScroll from 'locomotive-scroll';
// const scroll = new LocomotiveScroll({
//     el: document.querySelector('[data-scroll-container]'),
//     smooth: true
// });



// import AOS from 'aos';
// AOS.init();


// import anime from 'animejs/lib/anime.es.js';

// anime({
//     targets: '.bucket-container img:not(.bucket)',
//     // translateX: 250,
//     translateY: '200',
//     opacity: 1,
//     delay: anime.stagger(300, { easing: 'easeOutQuad' }),

//     duration: 250
// });

// var tl = anime.timeline({
//     easing: 'easeOutExpo',
//     duration: 300
// });

// // Add children
// tl
//     .add({
//         targets: '.piece-4',
//         translateY: 200,
//         opacity: 1,
//     })
//     .add({
//         targets: '.piece-3',
//         translateY: 200,
//         opacity: 1,
//     })
//     .add({
//         targets: '.piece-2',
//         translateY: 200,
//         opacity: 1,
//     })
//     .add({
//         targets: '.piece-1',
//         translateY: 200,
//         opacity: 1,
//     });
// import ScrollMagic from 'scrollmagic';

$(document).ready(function () {


    // var controller = new ScrollMagic.Controller();

//    //for section-slides-test
//    // define movement of panels
//     var wipeAnimation = new TimelineMax()
//         // animate to second panel
//         .to("#slideContainer", 0.5, { z: -150 })		// move back in 3D space
//         .to("#slideContainer", 1, { x: "-25%" })	// move in to first panel
//         .to("#slideContainer", 0.5, { z: 0 })				// move back to origin in 3D space
//         // animate to third panel
//         .to("#slideContainer", 0.5, { z: -150, delay: 1 })
//         .to("#slideContainer", 1, { x: "-50%" })
//         .to("#slideContainer", 0.5, { z: 0 })
//         // animate to forth panel
//         .to("#slideContainer", 0.5, { z: -150, delay: 1 })
//         .to("#slideContainer", 1, { x: "-75%" })
//         .to("#slideContainer", 0.5, { z: 0 });

//     // create scene to pin and link animation
//     new ScrollMagic.Scene({
//         triggerElement: "#pinContainer",
//         triggerHook: "onLeave",
//         duration: "500%"
//     })
//         .setPin("#pinContainer")
//         .setTween(wipeAnimation)
//         .addIndicators() // add indicators (requires plugin)
//         .addTo(controller);
    
    
    
    
   //for index.html
    // $('.pinned-section').each(function () {
    //     var pinIntroScene = new ScrollMagic.Scene({
    //         triggerElement: this,
    //         triggerHook: 0,
    //         duration: '110%',
    //         // offset: 588px,

    //     })
    //         .setPin(this, { pushFollowers: false })
    //         .addTo(controller);

    // });
    // var ourScene = new ScrollMagic.Scene({
    //     triggerElement: '#bucket-container',
    //     triggerHook: 0,
    //     // duration: '110%'
    // })

    //     .setClassToggle('.bucket-container', 'in-view')
    //     .addIndicators({
    //         name: 'chicken animation',
    //         colorTrigger: 'black',
    //         colorStart: 'black'
    //     })
    //     .addTo(controller);




    // var slideParallaxScene = new ScrollMagic.Scene({
    //     triggerElement: '.bcg-parallax',
    //     triggerHook: 1,
    //     duration: '50%'
    // })
    //     .setTween(TweenMax.from('.bcg', 1, { y: '-80%', ease: Power0.easeNone }))
    //     .addTo(controller);
    // var pinIntroScene = new ScrollMagic.Scene({
    //     triggerElement: '#intro',
    //     triggerHook: 0,
    //     duration: '100%'

    // })
    //     .setPin('#intro', { pushFollowers: false })
    //     .addTo(controller);

    // var pinFirstSection = new ScrollMagic.Scene({
    //     triggerElement: '#section-1',
    //     triggerHook: 0,
    //     duration: '100%'

    // })
    //     .setPin('#section-1', { pushFollowers: false })
    //     .addTo(controller);



    // var pinSecondSection = new ScrollMagic.Scene({
    //     triggerElement: '#section-2',
    //     triggerHook: 0,
    //     duration: '100%'

    // })
    //     .setPin('#section-2', { pushFollowers: false })
    //     .addTo(controller);


    // var pinThirdSection = new ScrollMagic.Scene({
    //     triggerElement: '#section-3',
    //     triggerHook: 0,
    //     duration: '100%'

    // })
    //     .setPin('#section-3', { pushFollowers: false })
    //     .addTo(controller);

    // var pinBucketSection = new ScrollMagic.Scene({
    //     triggerElement: '#bucket-container',
    //     triggerHook: 0,
    //     duration: '100%'

    // })
    //     .setPin('#bucket-container', { pushFollowers: false })
    //     .addTo(controller);



    

});